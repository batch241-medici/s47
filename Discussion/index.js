// The "document" refers to the whole webpage
// The "querySelector" is used to select a specific object (HTML element) from the document (webpage)
const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");

// Alternatively, we can use the getElement functions to retrieve the elements
// document.getElementById
// document.getElementByClassName
// document.getElementByTagName
// const txtFirstName = document.getElementById("txt-first-name");
// const spanFullName = document.getElementById("span-full-name");

// Whenever a user interacts with a web page, this action is considered as an event
// The "addEventListener" is a function that takes two arguments
// string identifying an event (keyup)
// function that the listener will execute once the specified event is triggered (event)
// keyup – fires when you release a key on the keyboard

txtFirstName.addEventListener('keyup', (event) => {
	// The innerHTML property sets or returns the HTML content
	spanFullName.innerHTML = txtFirstName.value;
	// The "event.target" contains the element where the event happened
	console.log(event.target);
	// event.target.value gets the value of the input object (similar to the txtFirstName.value)
	console.log(event.target.value);
});







